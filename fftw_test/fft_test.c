#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <fftw3.h>

/*
The base for this code has been yanked from ChatGPT,
Use with caution
*/

/*
Frequency band width (in Hz) = Sampling Frequency / Number of Samples
*/
#define SAMPLES 1000 // Number of samples, adjust as needed
#define SAMPLES_DATA "./data/512_audio_sample"
#ifdef REAL_DATA
    #undef SAMPLES
    #define SAMPLES 512
#endif

#define SAMPLING_FREQ 44100
#define BAND_WIDTH (SAMPLING_FREQ / SAMPLES)

double window[SAMPLES];
double window_sum = 0.0;

FILE *bands_output_file_ptr;
FILE *real_samples_file_ptr;

// Function to compute magnitude and convert to dB
void compute_magnitude_to_db(fftw_complex* fft_output, double* magnitude_db, int length) {
    // Skip bin[0] (FFT DC component)
    for (int i = 1; i < length; i++) {
        double magnitude = sqrt(fft_output[i][0] * fft_output[i][0] + fft_output[i][1] * fft_output[i][1]);

        magnitude *= 2.0;
        magnitude /= window_sum;

        magnitude_db[i] = 20 * log10(magnitude);
    }
}

void fill_audio_samples(double *audio_samples_arr) {
    real_samples_file_ptr = fopen(SAMPLES_DATA, "r");
    if (real_samples_file_ptr == NULL) {
        perror("File is NULL, check file path, or file contents");
        exit(EXIT_FAILURE);
    }
    
    int line_count = 0;
    char line[10];
    // Take sample from line, transpose and mod to float
    while (line_count < SAMPLES && fgets(line, sizeof(line), real_samples_file_ptr)) {
        int sample = atoi(line);
        // int16_t -> uint16_t -> double
        sample += 32768;
        double factorial_sample = (double) sample / 65535.0;

        audio_samples_arr[line_count] = factorial_sample;
        line_count++;
    }
    fclose(real_samples_file_ptr);
}

int main() {
    // Example audio samples (e.g., a sine wave)
    double audio_samples[SAMPLES];

    // Fill audio sample data from function or from file
#ifdef TEST_DATA
    for (int i = 0; i < SAMPLES; i++) {
        // Example: 440 Hz sine wave at 44.1 kHz sample rate
        audio_samples[i] = sin(2 * M_PI * 440 * i / SAMPLING_FREQ); 
    }
#endif
#ifdef REAL_DATA
    fill_audio_samples(audio_samples);
#endif

    // Generate Hanning window
    for (int i = 0; i < SAMPLES; i++) {
        window[i] = 0.5 * (1 - cos(2 * M_PI * i / (SAMPLES - 1)));
    }

    // Allocate memory for FFTW input and output
    fftw_complex *out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * SAMPLES);
    double *in = (double*) fftw_malloc(sizeof(double) * SAMPLES);
    
    // Copy audio samples to input array, apply window && window sum
    for (int i = 0; i < SAMPLES; i++) {
        in[i] = audio_samples[i] * window[i];
        window_sum += window[i];
    }

    // Create FFTW plan
    fftw_plan plan = fftw_plan_dft_r2c_1d(SAMPLES, in, out, FFTW_ESTIMATE);

    // Perform the FFT
    fftw_execute(plan);

    // Compute magnitude and convert to dB
    double magnitude_db[SAMPLES];
    // Take positive side of fft (SAMPLES / 2)
    compute_magnitude_to_db(out, magnitude_db, SAMPLES / 2);

    printf("Frequency data in dB:\n");
    bands_output_file_ptr = fopen("bands", "w");

    for (int i = 0; i < SAMPLES / 2 + 1; i++) {
        printf("Bin %d: %f dB, band: %d Hz\n", i, magnitude_db[i], BAND_WIDTH * i );
        fprintf(bands_output_file_ptr, "%f, %d\n", magnitude_db[i], BAND_WIDTH * i);
    }
    fclose(bands_output_file_ptr);

    // Free the FFTW plan and allocated memory
    fftw_destroy_plan(plan);
    fftw_free(in);
    fftw_free(out);

    return 0;
}