import matplotlib.pyplot as plt
import numpy as np

sample_lines = []

bands_dict = {}

with open('bands', 'r') as samples:
    sample_lines = samples.readlines()

for line in sample_lines:
    magnitude, band = line.strip().split(',')
    bands_dict[band] = float(magnitude)

labels = list(bands_dict.keys())
values = list(bands_dict.values())

fig, ax = plt.subplots()
ax.bar(labels, values)

ax.set_title("Audio samples")

plt.show()