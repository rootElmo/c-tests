# FFTW test

One (or more) test programs for testing **FFTW**.

## Files

`fft_test.c` - Simple test of audio sample -> frequency band conversion with dBFS conversion.

## Dependencies

[FFTW's](https://fftw.org/) official website.

```bash
# Debian based Linuxes
sudo apt udpate && sudo apt install libfftw3-dev
```

## Building

There are three different build option: `fftw-test`, `fftw-real-data`, `debug`.

`fftw-test`: Use a function to create samples of a 440Hz sine wave.

`fftw-real-data`: Use sampled data (`./data/512_audio_sample`).

`debug`: Same as `fftw-test`, but with debug flags for running the program with `gdb`.

Use `make` to build the program:

```bash
make fftw-test # choose the build option from above
```

## Output

The program will output the frequency bins to the `bands` file in plain text. You can use the `plot_bands_magnitude.py` to plot the data.

## Maintainers

Elmo Rohula